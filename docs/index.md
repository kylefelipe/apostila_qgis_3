# Bem-vindo ao Lado Livre da Força

![Divulgação Apostila](img/divulgacao_apostila.png)

A apostila tem o intuito de auxiliar na utilização no dia a dia do software Qgis, explicando suas funcionalidades e trazendo dicas
para melhor aproveitamento.
O conteúdo será melhorado com o passar do tempo, e contribuições são sempre bem vindas.

*Os dados utilizados nessa apostila encontram-se* [nesse reposiório](https://gitlab.com/kylefelipe/dados_apostila_qgis_3)

## Padrões

Colocar aqui as convenções da apostila

_`Português`_ (_`English`_) - O texto entre parênteses indica o texto original em inglês, para ajudar aqueles que usam o QGIS na linua original

_`Menu 1 > Sub Menu 1 > ...`_ - Menus e a ordem a serem acessados

__<botão\>__ - Indica o Botão a ser pressionado na interface

```
    # Linguagem do código
     {Codigo}
```
