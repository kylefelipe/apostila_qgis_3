# Instalando o [QGIS](www.qgis.org):

O [QGIS](www.qgis.org) foi concebido para ser um software multiplataforma, não obrigando o seu
usuário aficar preso a um determinado sistema operacional, dando opção inclusive
de o próprio usuário poder baixar os códigos fontes e compilar a instalação para
qualquer sistema que deseja usar, caso não haja algum instalador disponível para
o mesmo.
No site oficial, www.qgis.org, nos dá a opção de instalação para os seguintes Sistemas Operacionais:

*   Windows
*   Mac OS X

*   Linux, nas seguintes distribuições:

    *   Debian/Ubuntu
    *   Fedora
    *   openSUSE
    *   RHEL, CentOS, Scientific Linux, ...
    *   Mandriva
    *   Slackware
    *   ArchLinux
    *   BSD
    *   Android (__Exeprimental__)

A princípio vamos tratar aqui da instalação apenas no LINUX e WINDOWS, mas, contribuições serão bem vindas.

O QGIS possui algumas versões a serem instaladas, sendo elas:

*   __LTR__ - Long Term Release (Suporte Longo):  Versão recomendada para empresas, organizações e uso acadêmico, por receber mais correções de erros, sofrendo grandes atualizações apenas uma vez ao ano.
*   __LR__ - Latest Release (Lançamento Recente): Essa versão vem com as atualizações mais recentes testadas, recomendada para quem quer se manter atualizado, e sofre mudanças com mais frequência do que a LTR.
*   __Versões para Desenvolvedores__: Essa versão vem com as mais recentes atualizações, porém, com grandes poderes vem grandes responsabilidades, pois suas funcionalidades podem nem sempre funcionar como deveriam, causando quebras e fechamentos inesperado, trazendo problemas para quem precisa de um software mais estável.

Vamos adotar aqui a instalação LTR 3.4

## Instalação no LINUX (Debian/Ubuntu):

> "Come to the bash side..."

__ATENÇÂO:__ Esse material foi retirado do [SITE OFICIAL DO QGIS](www.qgis.org), sempre consulte em caso de problemas e dúvidas.
__ATENÇÂO__: O linux aceia apenas uma versão do [QGIS](www.qgis.org) instalada, sendo necessário remover instalações feitas previamente antes de realizar essa instalação.

Leia mais sobre [Linux](https://www.vivaolinux.com.br/linux/)

Essa instalação serve para as distribuições _DEBIAN_  e _UBUNTU_, outras deverão ser consultadas no site do [QGIS](www.qgis.org).
Para instalar no LINUX é necessário a senha do usuário ROOT, pois será necessário, editar a lista de repositórios, adicionar a chave ao sistema e saber o apelido da versão que do linux onde pretente instalar o [QGIS](www.qgis.org).
Acesso o terminal do Linux para proceder com todos os procedimentos de instalação. em alguns sabores, basta digitar CTRL+T para que o terminal apareça.

Para sabermos qual o apelido do sistema, basta digitar o seguinte comando no terminal:

```console
~$ lsb_release -a
```

A saída deve ser parecida com essa:
```console
~$ lsb_release -a
No LSB modules are available.
Distributor ID:	Debian
Description:	Debian GNU/Linux 9.4 (stretch)
Release:	9.4
Codename:	stretch
```

Nesse exemplo acima, o que nos interessa é a última linha _Codename_, pois é ela que vamos precisar para inserir o repositório corretamente, guarde-a.

O [QGIS](www.qgis.org) possui um padrão para os repositórios que é o seguinte:

__deb \*endereco_do_repositorio \*Codename \* main__
Sendo:
*   endereco_do_repositorio: Esse endereço pode ser escolhido na lista no site do [Qgis](https://qgis.org/en/site/forusers/alldownloads.html#debian-ubuntu).
*   Codename: apelido retornado no comando digitado anteriormente.

Por exemplo, o repositorio LTR para o _DEBIAN STRETCH_ ficaria aproximadamente assim:

> `deb https://qgis.org/debian stretch main`

Agora podemos editar a lista de repositorios do Linux para que possamos instalar e atualizar o [QGIS](www.qgis.org) quando necessário, para isso vamos precisar de um editor de texto, vou utilizar o __GEDIT__, fique a vontade para usar qualquer outro que esteja acostumado.
No terminal, logue como usuário root e digite:
```console
# gedit /etc/apt/sources.list
```
 E insira o repositorio na lista, conforme especificado acima.
 Caso queira, pode usar o simbolo \# para inserir um comentário em uma linha para saber o motivo do repositorio.
 Seu repositório ficará aproximadamente conforme a imagem abaixo:
![](../img/0repositorio.jpg)

Salve e feche o arquivo.
Para inserir a chave no sistema, para que o [QGIS](www.qgis.org) possa ser instalado, digite no terminal, como root:
```console
$ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key CAEB3DC3BDF7FB45
```
Para porceder com a instalação, digite no terminal, como root:

```terminal
# apt-get update
# apt-get install qgis python-qgis qgis-plugin-grass
```
Assim o [Qgis](www.qgis.org) será instalado no sistema LINUX e em poucos minutos estará pronto para ser executado.


## Instalação no Windows:

O [Windows](https://www.microsoft.com/pt-br/windows) é um dos sistemas mais comuns nos computadores pessoais no mundo. Nele é possível ter mais de uma versão do Qgis instalada ao mesmo tempo, e existem duas formas de instalação:

*   _Standalone Installer Version_: Essa é a versão de instalação simplificada, a que usaremos nessa apostila.

*   _OSGeo4W Network Instaler_: Instalação avançada do Qgis, é necessário conexão com a internet, e permite já instalar novas funcionalidades durante o processo.

Na página de downloads, vamos fazer o download da Versão _Long Term RElease_:

![](../img/download_windows.png)

Basta fazer o download conforme a arquitetura do processador e executar o arquivo baixado.
A instalação é bem simples, basta ir clicando em __Próximo__ nas telas que surgirem.

![](../../img/0tela1.PNG)
![](../../img/0tela2.PNG)
![](../../img/0tela3.PNG)
![](../../img/0tela4.PNG)
As opções dessa tela são referente aos dados de exemplo do QGIS, não é necessário fazer o download.

![](../../img/0tela5.PNG)
![](../../img/0tela6.PNG)
