# Configurando o QGIS.

O Qgis oferece diversas formas de customização, que podem atuar a nivel global, no software como um todo, ou apenas no projeto que está trabalhando.
Aqui vamos tratar apenas das configuraçoẽs globais, as de projeto ficará para um tópico específico sobre o projeto.

## Configurações Globais

As configurações feitas aqui reverberam no software como um todo, e nos projetos, novos e preexistentes.
Aqui podemos customizar Sistemas de Referencia de Coordenadas, Cores básicas utilizadas no projeto, teclas de atalho, aderência das camadas (snapping), idioma, proxy e outras customizações.

Tudo isso utilizando o menu _`Configurações`_ (_`Settings`_)

Vamos tratar de algumas configurações para nos ajudar na produção, acessando
_`Configurações > Opções`_ (_`Settings > Options`_)

![Configuração Global do QGis](../../img/04_configglobal_01.png)

Aqui temos:

1. Painel contendo as abas.
2. Tela de configuração da aba.
Para mudar entre as abas, basta clicar nos botão desejado no painel.
Há também os botões de __<Ajuda\>__, __<Cancelar\>__ e __<OK\>__, a posição deles varia de acordo com o Sistema Operacional que estiver usando.

#### Configurações > Opções > Gerais (General): ####

![Configurações Gerais](../../img/04_configglobal_02.png)

Nessa aba (_Override system locale_), podemos configurar em qual linguagem será exibida as telas, desde que haja tradução para a lingua.

Em _Aplicativo_ (_Application_), podemos configurar a aparência do Qgis o tema da interface, a fonte padrão utilizada, tamanho dos ícones, das fontes, o tempo de duração das mensagens que aparecem no Qgis.

Em _Arquivos de Projeto_ (_Project files_), podemos configurar se queremos que alguma tela de boas vindas, ou nenhuma, um padrão para novos projetos, e a possibilidade de utilzar macros.

#### Configurações > Opções > SRC (CRS): ####

![SRCs](../../img/04_configglobal_03.png)

Aqui vamos controlar qual o SRC em projetos novos, o comportamento quando uma camada não possui um SRC Definido e até mesmo transformações a serem feitas quando há SRCs diferentes.

Aqui devemos ter bastante atenção ao mudar alguma configuração, pois aqui podemos configurar os caminhos para pastas contendo SVG, caminhos para plugins, resetar configurações feitas pelo usuário, e configurar variáveis de ambiente.

#### Configurações > Opções > Fontes de Dados (Data Sources): ####

![Fontes de Dados](../../img/04_configglobal_04.png)

Aqui configuramos, a forma de como a tabela de atributos abre, a forma de cópia dos dados (quando se manda copiar um dado tabular ou vetorial), comportamento da tabela de atributos (se abre todas as feições, apenas selecionados...), dentre outras.

#### Configurações > Opções > Renderização (Rendering): ####

![Rederizar](../../img/04_configglobal_05.png)

Aqui vamos configurar como comporta a renderização dos dados no QGIS, quantidade de núcleos utilizados para isso, qualidade e por ai vai.

#### Configurações > Opções > Rede (Network): ####

![Rede](../../img/04_configglobal_06.png)

Aqui podemos configurar as especificidades da rede, principalmente quando estamos em ambientes corporativos, que precisam de proxy para acesso a internet.

#### Configurações > Atalhos de teclado ####

Acessando o menu _`Configurações > Atalhos de teclado (Settings > keyboard Shortcuts)`_ podemos configurar as teclas de atalhos para comandos, algoritimos e plugins.

![Atalhos de Teclado](../../img/04_configglobal_07.png)

Basta procurar na lista o que deseja inserir/alterar o atalho, clicar em __<Mudar\>__ e teclar o comando desejado.
Caso desejar utilizar o mesmo comando em várias maquinas, para não ter que ficar configurando cada atalho em todas elas, utilize o botão __<Salve\>__ __(<Save\>)__ para salvar um arquivo XML com todos os atalhos, e na maquina que desejar basta utilizar o botão __<Load\>__ para carregar o arquivo de atalhos customizados.
