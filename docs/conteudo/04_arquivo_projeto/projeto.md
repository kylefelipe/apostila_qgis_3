# O arquivo de PROJETO .qgs #

> \- Altera esse mapa pra mim?

> \- Tem o projeto?

> \- Que projeto?

> \- Xiiiiii!!!!!

O Arquivo de projeto é um dos arquivos mais importantes gerado pelo QGIS, ele permite que aquele lindo mapa que ficou por dois meses na caixa de entrada do chefe para aprovação e quando ele decide finalmente avaliar, solicita uma modificação?
Pois é, a existência ou não do arquivo de projeto é o que vai definir se vai se tornar um assassino ou se continua um cidadão de bem, feliz em seu emprego.
Com ele é possível fazer edições em mapas, dados e criar novos mapas para um mesmo trabalho feito.
Em fim... Sempre salve o arquivo de projeto junto com os dados, vidas podem ser salvas assim, e a sanidade do estagiário também.

![Aquivo de Projeto](../../img/05_propiedadesprojeto_01.png)

No menu _`Projeto`_ (_`Project`_) teremos tudo que for relativo ao projeto, como compositores de impressão, criação de novos projetos, exportação do projeto e as __Propriedades__ (__Properties__)
Acessando o menu _`Projeto > Propriedades`_  temos as janela para configurar o comportamento específico daquele projeto.

### Geral ###

_`Projeto > Propriedades do projeto > Geral`_

![](../../img/05_propiedadesprojeto_02.png)

Aqui podemos configurar as opções gerais do projeto, como ver o endereço do aquivo, o título do projeto, cor utilizada na seleção de dados, cor do fundo, a forma como ele salva o caminho dos arquivos.
Essa configuração merece uma atenção especial. Já tentou abrir um projeto feito em um pendrive e teve de configurar os dados todos de novo? Aqui podemos começar a tratar esse tipo de erro, utilizando a opção "relativo", colocando os arquivos a serem utilizados NA MESMA PASTA DO ARQUIVO DO PROJETO não é necessário ficar reconfigurando o caminho sempre que mudar a pasta de lugar, pois ele guarda o endereço relativo ao arquivo de projeto do Qgis.

Também podemos configurar como acontece as medições dentro do projeto, como serão exibidas as coordenadas e as escalas padrões do projeto.
