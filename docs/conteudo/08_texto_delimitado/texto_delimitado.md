# Arquivos de Texto Delimitado (CSV)

> \- Faz um mapa dessa propriedade pra mim?
>
> \- Ok, tem os vetores dela?
>
> \- Tenho esse memorial descritivo com as coordenadas.
>
> \- ...

É também muito comum utilizarmos arquivos, como planilhas, arquivos de texto, documentos, com coordenadas e precisamos desenhar esses dados no Qgis.
Para isso precisamos saber:

 Tipo de coordenada:

-   Geográfica: GMS - Grau Minutos e Segundos, DD - Grau Decimal
-   Plana: UTM - Universal Transversa de Mercator - Em Metros

Em SIG  temos um padrão para obedecermos ao informar as coordenadas nos sistemas:

*   Sempre informe na ordem: Longitude (X) e Latitude (Y).

O Qgis tem um padrão para leitura de coordenadas:

*   Geográficas - GMS:
 O padrão para Coordenadas GMS no QGIS é GGG MM SS.DDD.
 Ou seja GRAU separado de Minutos separado de Segunso por ESPAÇO, e o separador de decima é o ponto/virgula,
 e o indicador de hemisfério é o sinal de '-', e não as letras.

*   Geográficas - DD:
 O padrão para Coordenadas DD no QGIS é GGG.DDDDD
 Ou seja, Grau separado de Decimal por ponto/vírgula, e o indicador de hemisfério é o sinal de '-', e não as letras.

*   UTM:
  O padrão de coordenadas UTM no Qgis é MMMMMM.DDDDD
  Ou seja, Metros separado de Decimal por ponto/vírgula.

Perceba que em nenhum momento é utilizado separador de milhar.

Antes de adicionar os dados ao Qgis temos de verificar qual o tipo de coordenada, se há coordenadas com separador de decimal misturando ponto e virgula.
Remover separadores de milhar, caso haja.

Coordenadas UTM devemos ter a informação de FUSO (__NÃO CONFUNDA COM FUSO HORÁRIO__)

Após as verificações, podemos montar o nosso texto delimitado para inserir no QGIS (é aqui que a sanidade do estagiário é testada).

O Arquivo de texto delimitado é um arquivo onde os dados estão estruturados conforme um padrão, para isso utilizamos um DELIMITADOR, que irá separar os campos lá dentro.
Esse delimitador deve ser bem escolhido para não haver problemas, como separar as casas decimais de um dado, por exemplo.
Suites de escritório como o (Open/Libre/WPS) Oficce (CALC, MATH, Spreadsheets) (Livres) e MS Oficce (Excel) (Proprietária) tem a capacidade de salvar os arquivos no formato CSV - Comma-Separeated Values, Valores Separados por Vírgula, mas nada nos impede de abrir um simples editor de texto e montar o nosso próprio arquivo.

![](../../img/08_txtdelimitado_01.png)

Podemos ver na imagem acima, um arquivo de texto, onde:
 Podemos perceber que os dados são separados por ';'.
 A primeira linha contém o nome dos campos (vertice, x, y).
 As coordenadas estão em UTM.
 O separador decimal das coordenadas é a vírgula.

![](../../img/08_txtdelimitado_02.png)

Na imagem acima as coordenadas estão em uma estrutura um pouco diferente:
 Dados são separados por vírgula ','.
 Coordenadas em DD.
 Separador decima é o ponto '.'.
 Indicação de hemisfério é a presença do sinal de subtração '-'.

![](../../img/08_txtdelimitado_03.png)

Na imagem acima as cooredenadas estão em GMS.

## Abrindo o arquivo DELIMITADO

Para poder vermos as coordenadas no mapa, vamos precisamos importar os dados no QGIS.
Os passos são bem parecios com a importação de dados tabulares.


![](../../img/07_addarquivos_12.png)

A diferença está na configuração do dado:

### Coordenadas DD:

![](../../img/08_txtdelimitado_04.png)

Para as coordenadas em Grau Decimal, vamos marcar na caixa _Formato do arquivo (File Format)_ a opção __CSV__, em _Definição de Geometria (Geometry Definition)_ a opção __Coordenadas de ponto (Points Coordinates)__ e deixar desmarcada a opção  __Coordenadas GMS__ (__DMS COORDINATES__), repare nas configurações em __Formato do Arquivo__ (__File format__)
O QGIS verifica o nome dos campos e já faz a configuração dos campos 'Campo X' (X field) e 'Campo Y' (Y field), caso ele não reconheça, ou estiver com outro nome, basta indicar na lista.
E escolher o SRC da camada em __Geometry CRS__.

### Coordenadas GMS

![](../../img/08_txtdelimitado_05.png)

Nesse caso, marcamos a opção __Coordenadas GMS__.

### Coordenadas UTM

![](../../img/08_txtdelimitado_06.png)

Nesse arquivo foi adotada uma configuração um pouco diferente dos outros arquivos, esse arquivo, os dados são separados por _Ponto e vírgula (;)_ e o separador de decimal, a virgula, nesse caso, na caixa _Formato do arquivo_ marcamos a opção __Delimitadores Personalizados__ e dentro dele marcamos __Ponto e Vírgula__, já em _Opções de campos e dados (Records and Fields Options)_ vamos marcar a opção  __Separador decimal é a vírgula__ (__Decimal Separator is comma__).

Nesse caso, o que difere dos outros, básicamente, é o DATUM, pois as coodenadas estão em UTM.

Para isso, basta selecionar o SRC correto na caixa _Definição de Geometria (Geometry Definition)_ no campo __SRC da Geometria (Geometry SRS)__.


O Qgis permite apenas a leitura de arquivos de texto delimitados, caso precise editar algum, é necessário salvar salvar como arquivo vetorial para fazer tal edição.
Para isso, basta clicar sobre a camada com o botão direito ir em __Exportar (Export)__ > __Salvar Feições Como (Save Feature As)__:

![](../../img/08_txtdelimitado_07.png)

Na janela que abrir, basta configurar o arquivo de saida.

![](../../img/08_txtdelimitado_08.png)

Em __Formato (Format)__ escolhemos o formato de saída.
Em __Nome do Arquivo (File Name)__ Navegamos até a pasta onde queremos que o arquivo seja salvo, e damos um nome a ele.
Em __Nome da camada (Layer Name)__ Escolhemos um nome para a camada se ela for adicionada de volta ao projeto apos salva (Quando habilitado).
Em __SRC (CSR)__ Escolhemos o Sistema de Referência de Coordenada da camada.
Em __Codificação (Encoding)__ podemos escolher a codificação da tabela de atributos do arquivo de saida.
Em __Salvar apenas feições selecionadas (Save only selected features)__ caso tenhamos selecionado alguma feição dentro do nosso arquivo e queremos apenas ela no arquivo novo, caso contrario, desmarca essa opção. ela só será habilitada caso haja alguma feição selecionada.
Em __Selecionar campos para expotar e suas opções de exportação (Select fields to export and their export options)__ podemos escolher os campos a serem exportados.
