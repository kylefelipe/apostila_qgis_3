# Tabela de atributos.

A tabela de atributos contém os dados tabulares de uma feição, esses dados podem ser:
-   Textual, como nome, descrições e etc.
-   Numérico, sendo ele inteiro ou decimal
-   Data e Hora, nesse temos de ter cuidado para o padrão de data e hora aceito pelo formato, o padrão normalmente é yyyy-mm-dd (ano-mês-dia). Não são todos os formatos que aceitam esse tipo de dado, o SQLITE, presente no Geopackage e no Spatialite, por exemplo, dados do tipo data e hora são guardados em um campo de texto.

Alguns formatos, como banco de dados, tem um tipo de campo conhecido como [BLOB](https://pt.wikipedia.org/wiki/BLOB) - Binary Large OBject (Objeto Grande Binário) que pode armazenar imagens, pdf, audio, e etc.

_\- Mas como descobrir os tipos de dados que um arquivo aceita?_

_\- Basta procurar na documentação daquele arquivo, vou deixar uns links para os mais utilizados em Fonte._

Primeiro, precisamos selecionar a camada no painel de __CAMADAS__, assim, podemos acessar a tabela de diversas formas:

-   Tecla de atalho F6.

-   Botão __Abrir Tabela de Atributos (Open Attribute Tabel)__ na barra de ferramentas de atributos.

-   Clicar com o botão direito sobre a camada e clicar em __Abrir Tabela de Atributos (Open Attribute Tabel)__

![](../../img/10_tabelaatrib_01.png)

Exemplo de tabela de atributos:

![](../../img/10_tabelaatrib_02.png)

## Componentes:

Na tela da tabela, encontramos:

![](../../img/10_tabelaatrib_03.png)

 1.Barra de ferramentas:
 
*   __Habilitar Modo de Edição (Toggle Edit Mode)__  
*   __Habilitar Modo de Multi Edição* (Toggle Multi Edit Mode)__
*   __Salvar Edições* (Save Edits)__
*   __Atualizar Tabela (Reload Table)__
*   __Adicionar Feição* (Add Feature)__
*   __Deletar* (delete)__
*   __Selecionar Feições Por Expressão (Select Features Using Expressions)__
*   __Selecionar Todos (Select All)__
*   __Inverter Seleção (Invert Selection)__
*   __Remover Seleção (Deselect All)__
*   __Selecionar/Filtrar feições usando formulário (Select/Filter Features Using Form)__
*   __Mover Seleção Para o Topo (Move selection to top)__
*   __Mover o mapa para as linhas selecionadas (Pam map to the selected rows)__
*   __Zoom para feições selecionadas (Zoom map to the selected row)__
*   __Copiar feições selecionadas para área de trasnferência (Copy selected rows to clipboard)__
*   __Colar feições da área de transferência (Paste features form clipboard)__
*   __Novo Campo (New Field)__
*   __Deletar Campo (Delete Field)__
*   __Calculadora de Campo (Field Calculator)__
*   __Formatação Condicional (Conditional Formatting)__

    2.Escolha de campo a ser editado.*

    3.(Expression dialog).*

    4.Caixa de texto de entrada.*

    5.Opções de atualização:*
*   __Atualizar Todos (Update All)__
*   __Atualizar Selecionados (Update Selected)__

    6.Opções de filtro:
*   __Mostrar todas as Feições (Show all features)__
*   __Mostrar feições selecionadas (Show Selected Features)__
*   __Mostrar feições visiveis no mapa (Show Features Visible On Map)__
*   __Mostrar Feições Editadas e Novas (Show Edited and New Features)__
*   __Filtrar Campos (Filter Fields)__
*   __Filtro Avançado - Expressão (Advanced Filter - Expression)__

    7.Exibição da tabela de atributos:
*   __Mudar para vizualização de formulário (Switch to Form view)__
*   __Mudar para vizualização de tabela (Switch to table View)__

As opções com * só aparecem com a edição habilitada.

## Editando a Tabela:

Para editar uma tabela, é necessária que a edição da camada esteja habilitada.

### Inserindo campos.



## Calculadora de Campo.

A calculadora de campo é uma excelente ferramenta para se trabalhar a tabela de atributos, e ela está presente em todas as partes do Qgis, no [próximo capítulo](11_calculadora_de_campo.md) trata exclusivamente sobre ela.
