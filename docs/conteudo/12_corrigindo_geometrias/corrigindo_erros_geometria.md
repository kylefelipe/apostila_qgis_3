# ERROS DE GEOMETRIA

Erros de geometria são muito frequentes e causam muitas dores de cabeça durante o trabalho.
Ficar atento quanto a isso é uma coisa que devemos fazer o tempo todo, pois é uma boa prática para se ter uma qualidade de dado boa para trabalhar.

O Qgis possui uma ferramenta para auxiliar nessas correções, chama-se __VERIFICAR__ __GEOMETRIAS__

### SETUP:

![](../../img/12_erros_geom_01.png)

Com ela é possível fazer diversas verificações e correções que desejamos em nossas camadas, relacionados à geometria.

-   __Imput__ __vector__ __layer__: Aqui podemos escolher em quais camadas do projeto queremos fazer as verificações, basta deixar a caixa na frente da camada que deseja analizar marcada.
A opção "Apenas feições selecionadas" permite rodar as verificações apenas nas feições selecinadas dentro das camadas marcadas para serem verificadas.
-   [__Tipos__ __de__ __geometria__ __permitido__](12_corrigindo_erros_geometria.md#tipos-de-geometria-permitidos): Aqui podemos escolher os tipos de geometria que são permitidos dentro da camada.
-   [__Validade__ __da__ __geometria__](12_corrigindo_erros_geometria.md#Validade-da-geometria): Aqui podemos verificar problemas na forma da geometria. O verificador irá analizar se extiste cada uma das opções que invalidam uma geometria.
-   __Propriedades__ __da__ __Geometria__: Aqui podemos utilizar algumas regras de verificação, se há buracos em poligonos, ETC.
-   ndições presentes nas geometrias, como tamanho mínimo de cada segmento, angulo minimo entre os segmentos, área mínima permitida e fragmentos de poligonos.
-   __Verifica__ __topologia__: Aqui podemos validar algumas regras de topologia.
-   __Tolerância__: Aplica uma tolerância nas verificações.
-   __Output__ __vector__ __layers__: Aqui configuramos se vamos rodar as verificações e correções em um arquivo novo, ou nos arquivos originais.

Após marcarmos as opções que queremos, clicamos em _`Executar`_ para vermos o resultado das verificações.

### Resultado:

Nessa aba podemos ver o resultado das verificações escolhidas na aba _`SETUP`_

![](../../img/12_erros_geom_02.png)

-   __Resultado__ __do__ __teste__ __de__ __geometria__: Nesse painel irá aparecer cada um dos erros encontrados durante a verificação. O botão _`Exportar`_ gera um arquivo com os erros e a localização deles.
-   _`Quando uma linha é selicionada, mova a tela para`_: Aqui configuramos o comportamento quando clicamos em algum erro no painel do resultado.
-   _`Mostrar as feições selecionadas na tabela de atributos`_: Abre a tabela de atributos da feição que contem o erro selecionado, filtrando a camada.
-   _`Corrigir erros selecionados usando a resolução padrão`_: Aplica uma resolução automática, configurada em _`Configurações de resolução de erro`_.
-   _`Corrigir os erros selecionados, perguntar o método de resolução`_: Para cada erro selecionado no painel, irá aparecer uma tela solicitando a escolha da resolução.
-   _`Configurações de resolução de erro`_: Aqui podemos configurar uma resolução padrão.


## TIPOS DE GEOMETRIA PERMITIDOS

Aqui, conforme descrito acima, podemos verificar os tipos de geometrias dentro das camadas marcando os tipos de geometrias que queremos presentes nas camadas:

![](../../img/12_erros_geom_03.png)

Esse é o resultado das verificações:

![](../../img/12_erros_geom_04.png)

Formas de correção automática:

![](../../img/12_erros_geom_05.png)

*   _`Convert to correponding multi or single type if possible, otherwise delete feature`_: Converte a feição para o tipo multiparte ou partes simples correspondente, caso contrário deleta a feição.
*   _`Delete feature`_: Deletar a feição com erro.
*   _`No action`_: Fazer nada.

## Validade da geometria

Aqui fazemos a verificação da validade da geometria das camadas selecionadas.

![](../../img/12_erros_geom_06.png)

#### Auto Intercessões

![](../../img/12_erros_geom_07.png)

Esse é o resultado das verificações:

![](../../img/12_erros_geom_08.png)

Formas de correção automática:

![](../../img/12_erros_geom_09.png)

*   _`Split featura into a multi-object feature`_: Divide a feição gerando uma feição multiparte.
*   _`Split feature into multiple single-object features`_: Divide a feição em vários objetos simples.
*   _`No action`_: Fazer nada.

Obs: Deve-se ter cuidado ao utilizar a resolução automática, pois pode gerar dados inconsistentes, nem sempre é o mais indicado nesse caso.

#### Nós Duplicados

Ao contrário de auto intercessões, os nós duplicados são praticamente impóssiveis de serem vistos a olho nú.

![](../../img/12_erros_geom_10.png)

Esse é o resultado das verificações:

![](../../img/12_erros_geom_11.png)

Formas de correção automática:

![](../../img/12_erros_geom_12.png)

*   _`Delete duplicate node`_: Deletar nó duplicado.
*   _`No action`_: Fazer nada.

#### Contatos Automáticos

Contatos automáticos são provenientes de arestas da geometria que se tocam. Para esse erro o QGIS não apresenta nenhuma forma de resolução automática. Nesse caso, podemos utilizar a ferramenta de nós do programa para corrigir, basta deletar os nós.

![](../../img/12_erros_geom_13.png)

Esse é o resultado das verificações:

![](../../img/12_erros_geom_14.png)

#### Poligono com menos de 3 nós

Esse problema pode acontecer devido a problemas de vetorização, operação entre camadas. Tais operações podem deixar algum resíduo dentro da camada.
Tais feições não aparecem na visualização e geram erros de contagem e ETC.
A resolução automática para esse erro é a remoção da geometria e seus dados da tabela.

As outras opções serão inseridas em breve.
