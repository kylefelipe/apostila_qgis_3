# Criando e editando dados vetoriais.

O Qgis, a partir da versão 2.14, deu inicio o suporte a camadas temporárias de rascunho, e camadas virutais, o que ajuda muito na hora de criar camadas, já na versão 3.x adotou como formato padrão de dados o formato GEOPACKAGE (gpkg).

O interessante de se utilizar camadas virtuais/rascunho é a falta da necessidade de gerar uma quantidade grande de arquivos, tornando difícil saber qual era o correto.
Uma das características do Qgis é que podemos habilitar a edição apenas da camada que desejamos editar, isso evita que haja edição indesejada em arquivos errados.
Temos as seguinte ferramentas básicas para edição e criação de vetores:

![Imagem](../../img/09_criaeditavetores_01.png)

1. Barra de ferramentas de digitalização (Digitizing Toolbar):

    Aqui temos as seguintes ferramentas:

*   Edições Atuais (Current Edits).  
*   Abilitar Edição (Toggle Editing).  
*   Alvar Edições (Save Edits).  
*   Adicionar Feição (Add Feature).  
*   Adicionar String Circular (Add circular string).  
*   Mover Feição (Move Features).  
*   Ferramenta de Nó (Node tool).  
*   Deletar Selecionado (Delete Selected).  
*   Recortar feição (Cut features).  
*   Copiar feição (Copy Features).  
*   Colar feição (Paste Features).  
*   Numerical Digitizing.  


2.  Barra de Ferramentas de digitalização avançada (Advanced Digitizing Toolbar)

    Essa barra não vem habilitada por padrão.
    Para isso basta clicar com o botão direito em uma parte vazia na área de barras de ferramentas e clicar em  Ferramentas de digitalização avançada (Advanced Digitizing Toolbar):

![imagem](../../img/09_criaeditavetores_02.png)


    Aqui temos as seguintes ferramentas:


*   Habilitar Ferramentas Avançadas de Digitalização (Enable Advanced Digitizing Tools).
*   Habilitar Traçado (Enable Tracing).
*   Desfazer (Undo).
*   Refazer (Redo).
*   Rotacionar Feição (Rotate Feature(s)).
*   Simplificar Feição (Simplify feature).
*   Adicionar Anel (Add Ring).
*   Adicionar Parte (Add Part).
*   Preencher Anel (Fill Ring).
*   Deletar Anel (Delete Ring).
*   Deletar Parte (Delete Part).
*   (Reshape Feature).
*   (Offset Curve).
*   Dividir Feições (Split Features).
*   Dividir Partes (Split Parts).
*   Unir feições selecionadas (Merge Selected Features).
*   Unir atributos das feições selecionadas (Merge attributes of selected features).
*   Rotacionar Simbologia do ponto (Rotate Points Symbols).

## Camadas de Rascunho: ##

As camadas de rascunho ficam disponíveis apenas dentro do projeto que estamos trabalhando, sendo que com o fechamento do mesmo, elas são apagadas automaticamente.
Essa é também sua desvantagem, se a luz acaba, ou o projeto fecha antes de salvar a camada...
\#Choradeira.

\- Mas e se eu quiser manter os dados da camada de rascunho?
\- Botão direto sobre a camada > salvar como......

### Criando camadas de Rascunho: ###

Para criar camadas de rascunho é bem simples:

![](../../img/09_criaeditavetores_03.png)

Botão __Nova camada temporária de rascunho (New temporary scratch layer)__ (1), ou pelo menu  _`Camada (Layer) > Criar Camada (Create Layer) > Nova Camada Temporária de Rascunho (New temporary scratch layer)`_ (2).

![](../../img/09_criaeditavetores_04.png)

Na tela que abrir, podemos configurar:

1.  Tipo de geometria e SRC:

    -Tipo de Geometria: Ponto, Multiponto, Linha, Multilinha, Poligono, Multipoligono.
    -Sistema de Referencia de coordenadas: clicando no botão, ou selecionando na lista.
    Caso queira criar apenas uma tabela com dados, basta desmarcar a caixa Tipo de Geometria.

2.  Nome da Camada:
    Aqui escolhemos um nome para a camada.

Após escolher o tipo de camada, SRC e o nome da camada, basta clicar em __OK__ para que a camada seja criada, já com a edição habilitada.

Para adicionar uma feição basta clicar no botão __Adicionar Feição (Add Feature)__ e desenhar a forma desejada clicando na tela com o botão esquerdo. Após o último clique, basta clicar com o botão direito para terminar a edição.

![](../../img/09_criaeditavetores_05.png)

Para salvar as alterações na camada basta clicar no botão __Salvar Edições da Camada (Save Layers Edit)__

![](../../img/09_criaeditavetores_06.png)

Após todas as alterações terem sido feitas, basta salvar e fechar a edição clicando no __Alternar Edição (Toggle Editing)__

![](../../img/09_criaeditavetores_07.png)


### Editando uma camada: ###

#### Ferramentas de edição: ####

A edição de uma camada vetorial é bem simples.
Primeiro, habilitamos a camada para edição, e em seguida, selecionamos a ferramenta de edição que queremos utilizar (seja ela avançada ou não):

-   Ferramenta de nós(Node Tool):
Com ela podemos arrastar pontos, vértices ou arestas de geometrias dentro da camada, modificando a geometria.
Ao selecioná-la, devemos clicar sobre o ponto/vértice/aresta que desejamos arrastar

![](../../img/09_criaeditavetores_08.png)

Em seguida clicamos novamente na parte onde queremos alterar e arrastar para o local desejado.

![](../../img/09_criaeditavetores_09.png)

#### Ferramentas Avançadas

As ferramentas avançadas nos permitem cortar um vetor, unir, girar, dentre outros.

-   Recortando um poligono:
Para recortar um pologono, vamos utilizar a ferramenta __Dividir Feições (Split Features)__
Com ele basta desenhar a forma que queremos fazer o recorte:

![](../../img/09_criaeditavetores_10.png)

 E após fazer o ultimo clique, basta clicar com o botão direito para finalizar o corte.

 ![](../../img/09_criaeditavetores_11.png)

 __Atenção:__ O Qgis não edita alguns formatos vetoriais, como KML, CSV (textos delimitado) Basta salvar em um formato editável.

_\- Mas, se ao fecharmos os projeto, o meu arquivo de rascunho é deletado, como faço para persistir os dados?_  
_\- Simples, botão direito sobre a camada, salvar como..._
