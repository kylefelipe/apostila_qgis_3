# Adicionando arquivos ao Projeto

O Qgis, como já dito, nos permite trabalhar com diversos formatos de arquivos.
Para trabalharmos eles, precisamos adicionar ao projeto os arquivos necessários.

__ANTEÇÃO__ [Clique Aqui](https://gitlab.com/kylefelipe/dados_apostila_qgis_3) para baixar os arquivos a serem utilizados nessa apostila.

## Arquivos Vetoriais:

_Atenção_: Como base, vamos sempre utilizar arquivos vetoriais em formato GEOPACKAGE, mas vamos mostrar um pouco a utilização do _ESRI_ _SHAPEFILE_.

Para adicionarmos dados vetoriais no Qgis, temos algumas formas:

![](../../img/07_addarquivos_01.png)

Botão _Gerenciador de fontes de dados livres_ (1), menu _`Camada (layer)`_>_`Gerenciador de fontes de dados livres`_ (2) e menu _`Camada (layer)`_>_`Adicionar camada (Add Layer)`_ > _`Adicionar Camada Vetorial`_ (3) bem como a tecla de atalho _Ctrl+Shift+V_.

## Gerenciador de Fonte de Dados:

Uma das novidades do QGIS 3.x é o gerenciamento das fontes de dados em apenas uma tela, sem a necessidade de acessar vários caminhos diferentes para se adicionar dados ao projeto.

Através dessa tela é possível adicionar todos os formatos de com os quais o software trabalha.

![](../../img/07_addarquivos_02.png)

1.  _Painel de Fontes_: Aqui temos uma série de botões onde podemos escolher o provedor de dados que queremos adicionar.
2.  _Aba da fonte_: Aqui irá aparecer as opções de inserção dos dados.

#### Adicionar camada Vetor:

Por aqui podemos adicionar camadas vetoriais presentes nos mais diversos formatos suportados pelo QGIS.

![](../../img/07_addarquivos_03.png)

1.  _Tipo da Fonte (Source Type)_: Aqui escolhemos o tipo de formato que vamos abrir, arquivo, diretório, banco de dados, protocolo.
 Vamos deixar o arquivo marcado.

2.  _Codificação (Coding)_: Aqui, escolhemos a codificação da tabela de atributos, principalmente em formatos que não tem essa informação neles, como o SHAPEFILE.

3.  _Fonte (source)_: Aqui navegamos até a fonte do dado que queremos.

Ao clicar em _..._ na tela seguinte podemos navegar até a pasta onde encontra os arquivos que queremos abrir.

__Dica!__: Alguns formatos são formados por diversos arquivos, podemos utilizar o filtro de arquivo para encontrar com facilidade o que queremos abrir.

__Dica!__: Alguns formatos podem conter mais de uma camada vetorial, o qgis irá solicitar qual camada deseja inserir no projeto. Basta selecionar as camadas desejadas.

Um projeto com camadas vetoriais adicionadas fica com a aparência assim:

![](../../img/07_addarquivos_04.png)

Temos de ter em mente que a ordem das camadas no painel de camadas importa na hora de renderizar (desenhar) os dados na tela, ou seja, os dados que estão mais em cima são desenhados por cima, aqui vai uma dica para manter a organização das camadas na seguinte ordem:
*   Pontos;
*   Linhas;
*   Poligonos;
*   Rasters;

Sim, eu sei, ainda nem falei em como adicionar os rasters ainda... mas \#FiqueFirme.

Podemos reorganizar os dados no painel arrastando eles para cima ou para baixo.

\- Mas como saber se é um arquivo de ponto, linha ou poligono?
\- Boa pergunta, jovem gafanhoto...

Observe no painel de camadas, há um desenho entre o nome da camada e a caixa:

![](../../img/07_addarquivos_05.png)

Esse desenho indica o tipo de dado da camada:

![](../../img/07_addarquivos_06.png): Ponto
![](../../img/07_addarquivos_07.png): Linha
![](../../img/07_addarquivos_08.png): Poligono
![](../../img/07_addarquivos_09.png): Raster

Agora que já sabe identificar as camadas que foram adicionadas ao projeto, podemos mudar a ordem delas e testar como o Qgis renderiza as camadas.

## Dados tabulares:

Para adicionar dados tabulares no QGIs vai depender da forma que estão sendo distribuidos.

### Planilhas:

Para adicionar planilhas ao QGIS basta adicionar como se fossem arquivos vetoriais normais. Lembre de retirar o filtro de arquivo antes e de configurar a codificação correta para o arquivo a ser aberto.

Caso o arquivo contenha mais de uma planilha, o qgis irá solicita a escolha de quais tabelas devem se abertas.

### CSV:

Para adicionar aquivos tabulares em CSV, ou outro arquivo de texto delimitado, temos algumas formas:

![](../../img/07_addarquivos_10.png)

No _Gerenciador de Fonte de dados (Data Source Manager)_ clicamos em _Texto delimitado (Delimited text)_ conforme mostrado acima, ou no menu _`Camada (layer)`_ > _`Adicionar camada (Add Layer)`_ > _`A partir de um texto delimitado (Add Demited Text Layer)`_.

![](../../img/07_addarquivos_11.png)

Não há tecla de atalho, mas, o qgis é customisável, lembra?!

Na janela que abrir, basta navegar até o arquivo desejado em __Nome do Arquivo (File Name)__, clicando no "..." e depois configurar para que o arquivo apareça corretamente:

![](../../img/07_addarquivos_12.png)

Como estamos adicionando apenas um dado tabular, sem geometria, precisamos marcar a opção _Sem geometria (No geometry)_, veremos depois como adicionar arquivos delimitados com geometria.

\- E como eu identifico que a minha camada é um dado tabular?
\- Muito bem, padawan, dados tabulares aparecem da seguinte forma no painel de camadas:

![](../../img/07_addarquivos_13.png)

## Arquivos Matriciais (Rasters):

_Atenção!:_ Algumas imagens de satélite precisam de alguns procedimentos antes de serem adicionadas ao projeto, isso será tratado em um capitulo especial sobre dados matriciais.

Para adicionarmos arquivos Rasters ao Qgis, temos algumas formas:

![](../../img/07_addarquivos_14.png)

No _Gerenciador de Fonte de dados (Data Source Manager)_ clicamos em raster.
O menu _`Camada (layer)`_ > _`Adicionar camada (Add Layer)`_ > _`Adiconar Camada Raster (Add Raster Layer)`_ irá abrir o Gerenciador de Fonte de dados na aba RASTER, bem como com a tecla de atalho _Ctrl+Shift+R_.
E na janela que abrir, basta navegar até o arquivo desejado, clicando no botão _"..."_

![](../../img/07_addarquivos_15.png)

Aqui também podemos fazer filtragem de arquivos para facilitar.

![](../../img/07_addarquivos_16.png)

## Banco de dados:

Esse fica para um capitulo exclusivo também.
