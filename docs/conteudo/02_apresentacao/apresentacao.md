# A Cara do QGIS

Após instalado, é momento de colocarmos a mão na massa, e finalmente conhecer esse software que é tão falado e vem crescendo cada dia mais.
Para executá-lo, basta procurar pelo executavel dele no menu de programas de seu sistema operacional, sendo que no Windows, vão aparecer 3 executáveis:

*   Qgis Browser

*   Qgis Desktop

*   Qgis Desktop with grass

O Qgis Browser, é um navegador de arquivos, com ele podemos navegar pelo metadados dos arquivos espaciais, por bancos de dados, ajudando a gente a encontrar dados com mais facilidade.
O Qgis Desktop with grass é a versão do Qgis preparada para utilizar os algoritimos de outros software SIG, o [GRASS](https://grass.osgeo.org/).
Para fins didáticos, iremos utilizar o Qgis Desktop.
No Linux, só há a opção de executar o Qgis Desktop, que já vem preparado para o Grass.

## Idioma

Ao ser instalado, o qgis já adota como padrão o idioma do sistema operacional, cláro, caso haja tradução para aquele idioma.
Possui a maior parte da documentação e interface traduzida para o Portugês Brasileiro - Pt_br, e toda ajuda é bem vinda.

## Interface:

> Quem vê cara, não vê coração.

Essa é a cara do Qgis:

![](../../img/02_apresentacao_001.png)

O visual pode mudar conforme cada versão, e o usuário tem toda a liberdade de configurar da maneira que desejar também.
Está dividida nas seguintes partes:

1.  Barra de menus. Aqui teremos todos os menus existes no Qgis, e aqueles criados por algum plugin instalado.
2.  Barras de ferramentas. Aqui possui as ferramentas básicas do Qgis, Edição, manipulação do mapa, plugins e etc. E pode ser customizada conforme a vontade do usuário. Experimente arrastar uma das barras e posicionar em outro local...
3.  Painéis. Nessa parte ficam os painéis do Qgis, de Camadas (layers Panel), de Navegação, sejam eles padrão, ou criados por algum plugin, e também são customizáveis, tente posicioná-los em algum lugar da tela.
4.  Area de CAD. Essa parte é uma das principais do Qgis, pois é aqui que vai aparecer os dados geográficos adicionados ao Qgis. Aquele polígono dos municípios de Minas Gerais, aquela imagem de satélite baixada do [INPE](www.inpe.br) até mesmo aquele sobrevôo de vant feito.
5.  Barra de ferramenta de Camadas. Essa barra é uma barra comum, que para agilizar, por padrão foi colocada no canto da tela, mas pode ser movida para qualquer lugar.

Na parte de baixo do qgis fica a barra de coordenadas, escala, rotação, renderização, SRC e log de erros.

Quando já se tem projetos feitos no Qgis, eles aparecem listados na área de CAD, facilitando o acesso ao mesmo.

É nessa parte que vamos adicionar nossos vetores, tabelas, rasters, instalar plugins, rodar códigos [__Python__](www.python.org), codigos [SQL](https://pt.wikipedia.org/wiki/SQL).
Praticamente, 90% de tudo o que faremos será nessa parte do Qgis.

Portanto vamos por a mão na massa.
