# Site Oficial:  

*   www.qgis.org
*   www.qgisbrasil.org

# Forum/Lista de e-mail Oficial (BR):

*   https://groups.google.com/forum/#!forum/qgisbrasil

# Dados utilizados no curso:
Dados retirado da [IDE-SISEMA](http://idesisema.meioambiente.mg.gov.br/) em momentos diversos:

*   1104_mg_municipios_pol
*   2002_mg_unidades_conservacao_estaduais_pol
*   2002_MG_Unidades_Conservacao_Federais_pol
*   2002_mg_unidades_conservacao_municipais_pol
*   0902_mg_sede_municipal_pto
*   0402_mg_estacao_ferroviaria_pto
*   0402_mg_ferrovias_lin

Dados retirados do IBGE:

*   Limites_Estaduais_GCS

Imagem Landsat:

*   www.inpe.br

Imagem RAPIDEYE:

*   Instituto Estadual de Florestas  - IEF/MG

# Tutoriais e dicas:

*   www.kylefelipe.com
*   www.github.com/kylefelipe/qgis-tips-tricks
*   www.gitlab.com/kylefelipe/qgis-tips-tricks
*   www.andersonmedeiros.com
*   www.processamentodigital.com.br
*   [GEOCAST BRASIL](https://www.youtube.com/channel/UCLAeX4dyujMoy4xqHvxSDpQ)

# Grupos:

*   [Whatsapp](https://chat.whatsapp.com/E3HeXxP70bqFv8ybfbbV0W): https://chat.whatsapp.com/E3HeXxP70bqFv8ybfbbV0W
*   [Telegram](t.me/thinkfreeqgis): @thinkfreeqgis
