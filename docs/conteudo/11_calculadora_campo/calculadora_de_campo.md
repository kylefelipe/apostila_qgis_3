# Calculadora de Campo

A calculadora de campo é uma poderosa ferramenta desenvolvida dentro do QGIS, ela permite fazer extração de dados das geometrias dos vetores, inserir dados em campos baseados em regras, fazer somas, subtrações, divisões e outras inúmeras operações com os dados da camada. Podemos, inclusive, criar novas funções a serem utilizadas.
Todas as configurações dos campos variam conforme o formato da camada (Shapefile, Geopackage, Postgis, Spatialite...)

Devemos tomar alguns cuidados com a calculadora de campo:

*   Nomes de campo devem estar sempre entre aspas duplas (Ex.: `"campo_de_futebol"`).

*   Textos devem estar sempre entre aspas simples (`'Solto a vóz nas estradas, já não posso parar...'`)

*   Números: Podem ser inseridos diretamente na caixa de texto, devendo tomar cuidado para utilizar o ponto (.) como separador decimal, separador de milhar não deve ser utilizado. Exemplo: Inteiro `42`, decimal `3.14159265359`

*   Funções que começam com `$` não precisam de argumentos. Exemplo: $area -> Retorna a área da geometria.

*   Funções que começam com `@` normalmente acessam alguma variável. Exemplo: @row_number -> 1.

*   Funções que possuem parêntesis no final necessitam de parametros para funcionarem. Exemplo: __y(__`$geometry`__)__ -> Retorna o valor do eixo y da feição.

As funções podem ser combinadas para atingir o resultado desejado.

![](../../img/11_calculadora_campo_01.png)

 1._`Atualizar apenas X feições selecionadas`_: Essa opção é habilitada quando há alguma seleção na camada. Ela faz com que as alterações aconteçam apenas nas feições selecionadas. Caso queira que aconteça para todas as feições, essa opção deve ser desmarcada.
 O x representa a quantidade de feições selecionadas dentro da camada.


 2.Caixa _`Criar um novo campo`_: Aqui podemos configurar para que seja criado um novo campo na camada.
*   _`Criar um campo virtual`_: Essa opção permite que seja criado um campo virtual na camada a ser editada, sendo que o campo ficar apenas dentro do projeto do QGIS.
*   _`Nome do campo`_: Aqui podemos determinar o nome do novo campo a ser criado na camada.
*   _`Tipo do novo campo`_: Aqui configuramos o tipo que o campo irá ter quando criado.
*   _`Comprimento do campo de saida`_: Aqui informamos o tamanho do campo a ser criado.
*   _`Precisão`_: Aqui configuramos a quantidade de casas decimais do campo, se a configuração o permitir.
*   As medições feitas levão em consideração as [configurações do projeto](05_projeto.md) relativas a forma de medição configurada.


 3.Caixa _`Atualiza um campo existente`_: Aqui podemos escolher na lista um campo que queremos atualizar que já exista na camada.


 4.Aba _`Expressão`_: Aqui podemos construir expressões para inserir, modificar dados na camada.
     a) _`Caixa de texto`_: Aqui montamos a expressão utilizando as funções presentes. Acima dessa caixa aparece alguns operadores que podemos utilizar ao construir expressões, os operadores pondem ser inseridos na expressão utilizando o telado também.
     b) _`Lista de funções`_: Aqui temos todas as funções que podemos utilizar na camada, desde nomes de campos da tabela, até manipulação de texto.
     c) __Ajuda__: Aqui verificamos a ajuda da expressão selecionada na lista de funções, aqui também pode mostrar uma amostra do que está no campo selecionado na tabela de atributos, ou todos os valores únicos.
     d) __Prévia de saída__: Aqui aparece uma amostra do resultado da expressão criada, caso haja algum erro, aqui irá aparecer _Expressão Inválida <u>(more info)</u>_.


 5.Aba _`Editor de funções`_: Nessa aba, é possível acrescentar novas funções, construidas utilizanto Python.


 Algumas funções são muito utilizadas, e vamos passar por uma delas.

##### Grupo `Campos e valores`:
 ![](../../img/11_calculadora_campo_02.png)
 Aqui aparecem todos os campos presentes na tabela, com exceção da coluna que contém geometria. Com duplo click é possível adicionar o campo na caixa de texto. Caso queira inserir o nome do campo através do teclado, sempre devemos utilizar as aspas duplas (Ex.: "nome do campo") para evitar que seja tratado como uma função.


##### Grupo `Geometria`:
Aqui estão presentes as funções que utilizam a geometria da camada.
![](../../img/11_calculadora_campo_03.png)

-   `||`: Faz concatenação de texo e/ou campos. Exemplo: 'brincando' || 'com' || 'textos' -> 'Brincando com texos'
-   _$geometry_: Retorna a geomertia da feição.
-   _$id_: Retorna o identificador único (id/fid/ogc_id) da feição dentro da camada.
-   x_min(geometria): Retora o menor valor do eixo x da feição, de acordo com o SRC da camada. Exemplo: x_min($geometry)
-   x_max(geometria): Retora o maior valor do eixo x da feição, de acordo com o SRC da camada. Exemplo: x_max($geometry)
-   y_min(geometria): Retora o menor valor do eixo y da feição, de acordo com o SRC da camada. Exemplo: y_min($geometry)
-   xymax(geometria): Retora o maior valor do eixo y da feição, de acordo com o SRC da camada. Exemplo: y_max($geometry)

Funções referentes à polígonos:
-   _$area_: Retorna a área da geometria. Exemplo: $area
-   _area(geometria)_: Retorna a area da geometria também, porém sempre planilmétrica, no SRC da camada. Exemplo: area($geometry)
-   _$perimeter_: Retorna o perímetro da feição. $perimeter
-   _centroid(geometria)_: Retorna o centro gemétrico de um polígono. Exemplo: centroid($geometry)
-   point_on_surface(geometria): Retorna um ponto dentro da geometria. Exemplo($geometry)

Funções referentes à linhas:
-   _$length_: Retorna o tamanho da linha. $length
-   _length()_: Retorna o tamanho da linha, porém sempre planilmétrica, no SRC da camada. Exemplo: length($geometry).

Funções referentes à pontos:
-   _$y_: Retorna o eixo y da geometria do ponto.
-   _y(geometria)_: Retorna o eixo y de uma geometria, ou o eixo y do centroide de uma geometria. Exemplo: y($geometry).
-   _$x_: Retorna o eixo x da geometria do ponto.
-   _y(geometria)_: Retorna o eixo x de uma geometria, ou o eixo x do centroide de uma geometria. Exemplo: x($geometry).
-   _m(geometria)_: Retorna a dimensão m de um ponto. Exemplo: m($geometry)
-   _z(geometria)_: Retona a dimensão z de um ponto. Exemplo: z($geometry)

Após a criação da expresão, ao clicar em _`OK`_, caso não haja nenhum erro, os valores serão inseridos na tabela de atributos, no campo novo ou no campo já existente, dependendo da configuração.

Exemplo de expressão:

![](../../img/11_calculadora_campo_04.png)

As funções possuem ajuda muito bem definidas, e há vários plugins que acrescentam novas funções à calculadora e novas funções podem ser encontradas na internet.
Deve-se verificar a compatibilidade das funções com a versão do Python utilizada pelo Qgis.

Novas funções para o Qgis:

[Repositório Qgis Tips & Tricks](https://github.com/kylefelipe/qgis-tips-tricks/tree/master/python/funcoes_qgis)
[Funções IBAMA](https://github.com/lmotta/ibama_expressions)
