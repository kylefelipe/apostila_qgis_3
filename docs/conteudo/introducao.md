# Uma breve história

Em Maio de 2002 foi dado início a um projeto que visava dar acesso a qualquer
pessoa que possuisse um computador acesso a um software de geoprocessamento,
que até então, era um campo que possuia softwares caros, limitando a sua
utilização a empresas e organizações que possuiam poder aquisitivo.
Assim nasceu o Quantum Qgis, que depois de 10 anos, adotou como nome o apelido
dado pelos seus utilizadores, QGIS.
Com a filosofia do Free Open Source Software - FOSS (Software Livre e de Codigo Aberto)
o Qgis tem se destacado no mercado, sendo lider nas pesquisas realizadas no Google.

Tem a premissa de ter uma interface amigável para quem utiliza, e multi-plataforma,
e sua idéia inicial era ser um visualizador de dados espaciais, mas com sua facilidade
de customização e a necessidade de ferramentas cada vez melhor, foi crescendo,
juntamente com outros projetos, sendo apoiado pela Fundação Geoespacial de Código Aberto - OSGEO.

Hoje é possivel utilizar no Qgis a grande maioria dos dados espaciais existentes,
principalmente aqueles em formatos abertos, como Geopakage, KML, TIFF e etc.
E é distribuido sobre a GNU General Public License (GPL) que permite que qualquer
possa distribuir o QGIS de forma livre, bem como, modificá-lo e divulgar tais
modificações para a comunidade.

Com a nova versão 3.X veio profundas mudanças, tais como, adoção do python 3 como base, mudança para a biblioteca QT5 para a interface do usuário, dentre outras. Tais mudanças que afetam os plugins desenvolvidos, que devem ser portados para a nova versão.
Muitos já foram portados, e alguns deles já não oferecem mais atualizações na versão 2.18, recebendo suporte apenas para a versão 3.X.
