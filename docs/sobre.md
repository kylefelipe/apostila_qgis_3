# Sobre

![Divulgação Apostila](img/divulgacao_apostila.png)

A apostila tem o intuito de auxiliar na utilização no dia a dia do software Qgis, explicando suas funcionalidades e trazendo dicas
para melhor aproveitamento.
O conteúdo será melhorado com o passar do tempo, e contribuições são sempre bem vindas.

Os dados utilizados nessa apostila encontram-se em [https://gitlab.com/kylefelipe/dados_apostila_qgis_3](https://gitlab.com/kylefelipe/dados_apostila_qgis_3)
